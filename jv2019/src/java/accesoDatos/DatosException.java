/** 
Proyecto: Juego de la vida.
 * Clase de excepción para la gestión de los errores 
 * en el almacén de datos.
 * @since: prototipo 0.1.2
 * @source: DatosException.java 
 * @version: 0.1.2 - 2020/02/25
 * @author: ajp
 */

package accesoDatos;

public class DatosException extends RuntimeException {

	public DatosException(String mensaje) {
		super(mensaje);
	}
	
	public DatosException() {
		super();
	}
}
