/** 
Proyecto: Juego de la vida.
 * Clase especializada en el acceso a datos de simulaciones utilizando un arraylist
 * @since: prototipo 0.1.1
 * @source: SimulacionesDAO.java 
 * @version: 0.2.0 - 2020/05/02
 * @author: josemaelmismo (Jose Manuel Monteagudo) GRUPO 4
 * @author: SpikeySP (Juan Ignacio Riquelme Martinez) GRUPO 4
 */

package accesoDatos.memoria;

import java.util.ArrayList;
import java.util.List;

import accesoDatos.DatosException;
import accesoDatos.OperacionesDAO;
import modelo.Identificable;
import modelo.Simulacion;


public class SimulacionesDAO implements OperacionesDAO {

	private static SimulacionesDAO instancia = null;
	private static ArrayList<Simulacion> datosSimulaciones;

	private SimulacionesDAO() {
		datosSimulaciones = new ArrayList<>();
	}

	public static SimulacionesDAO get() {
		if (instancia == null) {
			instancia = new SimulacionesDAO();

		}
		return instancia;
	}

	public ArrayList<Simulacion> getSimulaciones() {
		return datosSimulaciones;
	}

	public Simulacion obtener(String id) {
		for (Simulacion simulacion : datosSimulaciones) {
			if (simulacion != null && simulacion.getId().equalsIgnoreCase(id)) {
				return simulacion;
			}
		}
		return null;
	}

	public List<Object> obtenerTodos() {
		return null;
	}

	public void alta(Object obj) throws DatosException {
		assert obj != null;
		Simulacion simulacion = (Simulacion) obj;
		int posicionInsercion = indexSort(simulacion.getId());

		if (posicionInsercion < 0) {
			datosSimulaciones.add(Math.abs(posicionInsercion) - 1, simulacion);
		} else {
			throw new DatosException("Alta Simulacion: ya existe");
		}

	}

	private int indexSort(String idSimulacion) {
		int size = datosSimulaciones.size();
		int puntoMedio;
		int limiteInferior = 0;
		int limiteSuperior = size - 1;

		while (limiteInferior <= limiteSuperior) {
			puntoMedio = (limiteSuperior + limiteInferior) / 2;
			int comparacion = idSimulacion.compareTo(datosSimulaciones.get(puntoMedio).getId());

			if (comparacion == 0) {
				return puntoMedio + 1;
			}

			if (comparacion > 0) {
				limiteInferior = puntoMedio + 1;
			} else {
				limiteSuperior = puntoMedio - 1;
			}
		}
		return -(limiteInferior + 1);
	}

	public Object baja(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void borrarTodo() {
		// TODO Auto-generated method stub

	}

	@Override
	public Object actualizar(Object obj) throws DatosException {
		assert obj != null;
		Simulacion simulActualizada = (Simulacion) obj;
		int posicion = indexSort(simulActualizada.getId());

		if (posicion > 0) {
			datosSimulaciones.set(posicion - 1, simulActualizada);
		} else {
			throw new DatosException("Actualizar simulación : No existe la simulación a actualizar");
		}
		return simulActualizada;

	}

	@Override
	public String toStringDatos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toStringIds() {
		// TODO Auto-generated method stub
		return null;
	}

	public int size() {
		return datosSimulaciones.size();
	}

	/**
	 * Metodo que inserta en el array de simulaciones
	 * 
	 * @param idSimulacion - id de la simulacion a insertar
	 * @return posicion en la que insertar la simulacion
	 */

	public Object obtener(Object obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object consultar(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Identificable> consultarTodos() {
		// TODO Auto-generated method stub
		return null;
	}

}