/** 
 * Proyecto: Juego de la vida.
 *  Resuelve el acceso a los datos de los usuarios utilizando un ArrayList
 *  y un HashMap.
 *  Aplica el patrón Singleton.
 *  Participa en el patrón Façade.
 *  @since: prototipo 0.2.0
 *  @source: UsuariosDAO.java 
 *  @version: 0.2.0 - 2020/04/23
 *  @author AlbertoGonzalez - Grupo 5
 */

package accesoDatos.memoria;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import accesoDatos.DatosException;
import accesoDatos.OperacionesDAO;
import config.Configuracion;
import modelo.ClaveAcceso;
import modelo.Correo;
import modelo.DireccionPostal;
import modelo.Identificable;
import modelo.ModeloException;
import modelo.Nif;
import modelo.Usuario;
import util.Fecha;
import util.Formato;

public class UsuariosDAO extends IndexSortTemplate implements OperacionesDAO {

	// Singleton
	private static UsuariosDAO instance;
	
	// Elementos de almacenamiento
	private ArrayList<Identificable> datosUsuarios;
	private HashMap<String, String> equivalenciasId;
	
	/**
	 * Método estático de acceso a la instancia única.
	 * Si no existe la crea invocando al constructor interno.
	 * Utiliza inicialización diferida.
	 * Sólo se crea una vez; instancia única -patrón singleton-
	 * @return instance
	 */
	public static UsuariosDAO getInstance() {
		
		if (instance == null) instance = new UsuariosDAO();
		
		return instance;
	}
	
	/**
	 * Constructor por defecto de uso interno.
	 * Sólo se ejecutrá una vez.
	 */
	private UsuariosDAO() {
		
		datosUsuarios = new ArrayList<Identificable>();
		equivalenciasId = new HashMap<String,String>();
		cargarUsuariosIntegrados();
	}
	
	private void cargarUsuariosIntegrados() {

		Usuario admin = null;
		
		try {
			
			Usuario invitado = new Usuario();
			
			alta(invitado);

			admin = new Usuario(new Nif(Configuracion.get().getProperty("usuario.adminNif")),
					Configuracion.get().getProperty("usuario.admin"), 
					Configuracion.get().getProperty("usuario.admin") + " " + Configuracion.get().getProperty("usuario.admin"),
					new DireccionPostal(),
					new Correo(Configuracion.get().getProperty("usuario.admin") + Configuracion.get().getProperty("correo.dominioPredeterminado")),
					new Fecha().addYears(-Integer.parseInt(Configuracion.get().getProperty("usuario.edadMinima"))),
					new Fecha(),
					new ClaveAcceso(Configuracion.get().getProperty("usuario.adminClaveAcceso")),
					Usuario.RolUsuario.ADMIN
					);
			
			alta(admin);
			
		} catch (ModeloException e) {
			
			e.printStackTrace();
			
		} catch (DatosException e) {
			
			e.printStackTrace();
		}	
		
	}

	/**
	 * Obtiene un Usuario dado su identificador.
	 * @param id - el identificador del objeto a buscar.
	 * @return - el objeto encontrado o null si no existe.
	 */
	@Override
	public Usuario consultar(String id) {
		
		int index = indexSort(datosUsuarios, equivalenciasId.get(id));
		
		if (index < 0) {
			
			return null;
		}
		
		return (Usuario) datosUsuarios.get(index-1);
	}

	/**
	 * Obtiene todos los obejetos almacenados en una lista de identificables.
	 * @return - La lista.
	 */
	@Override
	public List<Identificable> consultarTodos() {
		
		return datosUsuarios;
	}

	/**
	 *  Alta de un objeto en el almacén de datos, 
	 *  sin repeticiones, según el campo id previsto. 
	 *	@param obj - el objeto a almacenar.
	 *  @throws DatosException - si ya existe.
	 */
	@Override
	public void alta(Object obj) throws DatosException {
		
		assert obj != null;
		Usuario usr = (Usuario) obj;
		
		int index = indexSort(datosUsuarios, usr.getId());
		
		if (index < 0) {
			
			// index es en base 1
			datosUsuarios.add(-index-1, usr);
			altaEquivalId(usr);
			
		} else {
			
			int cont = 1;
			
			do {
				
				cont ++;
				usr.VariarId();
				index = indexSort(datosUsuarios, usr.getId());
				
				if (index < 0) {
					
					// index es en base 1
					datosUsuarios.add(-index-1, usr);
					altaEquivalId(usr);
					
					return;
				}
				
			} while (cont < Formato.LETRAS_NIF.length());
			
			throw new DatosException("Datos.altaUsuario: Id en uso...");
		}
		
	}
	
	private void altaEquivalId(Usuario usr) {
		
		equivalenciasId.put(usr.getId(), usr.getId());
		equivalenciasId.put(usr.getNif().getTexto(), usr.getId());
		equivalenciasId.put(usr.getCorreo().getTexto(), usr.getId());	
	}

	/**
	 * Elimina el objeto, dado el id utilizado para el almacenamiento.
	 * @param id - el identificador del objeto a eliminar.
	 * @return - el Objeto eliminado.
	 * @throws DatosException - si no existe.
	 */
	@Override
	public Usuario baja(String id) throws DatosException {
		
		int index = indexSort(datosUsuarios, id);
		
		if (index > 0) {
			
			Usuario usr = (Usuario) datosUsuarios.get(index-1);
			
			// index es en base 1
			datosUsuarios.remove(index-1);
			bajaEquivalId(usr);
			
			return usr;
		}
		
		throw new DatosException("Datos.bajaUsuario: el usuario no existe...");	
	}
	
	private void bajaEquivalId(Usuario usr) {
		
		equivalenciasId.remove(usr.getId());
		equivalenciasId.remove(usr.getNif().getTexto());
		equivalenciasId.remove(usr.getCorreo().getTexto());		
	}

	/**
	 *  Actualiza datos de un Objeto reemplazando el almacenado por el recibido.
	 *	@param obj - el objeto nuevo.
	 *  @return - el Objeto modificado.
	 *  @throws DatosException - si no existe.
	 */
	@Override
	public Usuario actualizar(Object obj) throws DatosException {
		
		assert obj != null;
		Usuario usr = (Usuario) obj;
		
		int index = indexSort(datosUsuarios, usr.getId());
		
		if (index > 0) {
			
			Usuario aux = (Usuario) datosUsuarios.get(index-1);
			
			// index es en base 1
			datosUsuarios.set(index-1, usr);
			bajaEquivalId(aux);
			altaEquivalId(usr);
			
			return aux;
		}
		
		throw new DatosException("Datos.modificarUsuario: el usuario no existe...");
	}

	/**
	 * Obtiene el listado de todos los datos almacenados.
	 * @return el texto con el volcado de datos.
	 */
	@Override
	public String toStringDatos() {
		
		return toStringAlmacen(1);
	}

	/**
	 * Obtiene el listado de todos id's de los objetos almacenados.
	 * @return el texto con el volcado de id.
	 */
	@Override
	public String toStringIds() {
		
		return toStringAlmacen(2);
	}
	
	private String toStringAlmacen(int select) {
		
		StringBuilder result = new StringBuilder();
		
		for (Identificable usr: datosUsuarios) {
			
			switch (select) {
				
				case 1: 
					result.append("\n" + usr);
					break;
					
				case 2: 
					result.append("\n" + usr.getId());
					break;
					
				default: 
					System.err.println("El índice seleccionado no es válido");
			}
			
		}
		
		return result.toString();
	}

	/**
	 * Elimina todos los datos y restaura los valores predeterminados.
	 */
	@Override
	public void borrarTodo() {
		
		datosUsuarios.clear();
		equivalenciasId.clear();
		cargarUsuariosIntegrados();
	}
	
}
