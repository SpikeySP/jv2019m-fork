/** Proyecto: Juego de la vida.
 *  Acceso datos de Mundos
 *  @since: prototipo2.0
 *  @source: MundosDAO.java 
 *  @version: 0.2.0 - 2020/04/21
 *  @author: axiel7 (Grupo2)
 */
package accesoDatos.memoria;

import java.util.ArrayList;
import java.util.List;

import accesoDatos.DatosException;
import accesoDatos.OperacionesDAO;
import modelo.Identificable;
import modelo.Mundo;
import modelo.Sesion;

public class MundosDAO implements OperacionesDAO{

	private static MundosDAO instancia = null;
	private ArrayList<Identificable> datosMundos;

	private MundosDAO() {
		datosMundos = new ArrayList<>();
		cargarPredeterminados();
	}

	public static MundosDAO getInstancia() {
		if (instancia == null) {
			instancia = new MundosDAO();
		}
		return instancia;
	}

	@Override
	public Object consultar(String id) {
		for (Identificable mundo : datosMundos) {
			if (mundo != null && mundo.getId().equalsIgnoreCase(id)) {
				return mundo;
			}
		}
		return null;
	}

	@Override
	public List<Identificable> consultarTodos() {
		return datosMundos;
	}

	@Override
	public void alta(Object obj) throws DatosException {
		assert obj != null;
		Mundo mundo = (Mundo)obj;
		int posicion = indexSort(datosMundos, mundo.getId());

		if (posicion < 0) {
			datosMundos.add(Math.abs(posicion) - 1, mundo);
		} 
		else {
			throw new DatosException("Error: nombre del mundo repetido");
		}
		
	}

	@Override
	public Object baja(String id) {
		int index = indexSort(datosMundos, id);
		int pos = index - 1;
		if (index > 0) {
			Mundo auxMundo = (Mundo) datosMundos.get(pos);
			datosMundos.remove(pos);
			return auxMundo;
		}
		throw new DatosException("Error al dar de baja: no existe el mundo.");	
	}

	@Override
	public String toStringIds() {
		StringBuilder result = new StringBuilder();
		for (Identificable mundos : datosMundos) {
			result.append("\n" + mundos.getId());
		}
		return result.toString();
	}
	
	@Override
	public String toStringDatos() {
		StringBuilder result = new StringBuilder();
		for (Identificable mundos : datosMundos) {
			result.append("\n" + mundos);
		}
		return result.toString();
	}

	@Override
	public void borrarTodo() throws DatosException {
		datosMundos.clear();
		cargarPredeterminados();
		
	}
	
	@Override
	public Object actualizar(Object obj) {
		// TODO Auto-generated method stub
		Sesion mundo = (Sesion) obj;
		int index = indexSort(datosMundos, mundo.getId());
		int pos = index - 1;
		if (index > 0) {
			Mundo auxMundo = (Mundo) datosMundos.get(pos);
			datosMundos.set(pos, mundo);
			return auxMundo;
		}
		throw new DatosException("Error al actualizar: no existe el mundo.");
	}

	public int size() {
		return datosMundos.size();
	}
	
	/**
	 * Busca usuario por su nif (id).
	 * @param datosMundos
	 * @param usrId - nif del Usuario a buscar.
	 * @return - el Usuario encontrado o null.
	 */
	private int indexSort(ArrayList<Identificable> datosMundos, String usrId) {
		int size = datosMundos.size();
		int puntoMitad;
		int limiteInferior = 0;
		int limiteSuperior = size - 1;

		while (limiteInferior <= limiteSuperior) {
			puntoMitad = (limiteSuperior + limiteInferior) / 2;
			int comparacion = usrId.compareTo(datosMundos.get(puntoMitad).getId());

			if (comparacion == 0) {
				return puntoMitad + 1;
			}

			if (comparacion > 0) {
				limiteInferior = puntoMitad + 1;
			} 
			else {
				limiteSuperior = puntoMitad - 1;
			}

		}
		return Math.abs(limiteInferior + 1);
	}

	public void cargarPredeterminados() throws DatosException {
		
		byte [][] espacioDemo = new byte[][] { 
										{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
										{ 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
										{ 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
										{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } };

		Mundo mundo = new Mundo();
		mundo.setEspacio(espacioDemo);
		alta(mundo);
	}
}
