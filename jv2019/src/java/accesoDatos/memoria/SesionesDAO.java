/** 
 * Proyecto: Juego de la vida.
 * Resuelve el acceso a los datos de las sesiones de usuarios 
 * incluyendo los mecanismos necesarios para almacenarlas en un ArrayList.
 * @since: prototipo 0.1.1
 * @source: SesionesDAO.java 
 * @version: 0.2.0 - 2020/05/02
 * @author: neuck (Christian López) GRUPO 1
 * @author: daviz89 (David González) GRUPO 1
 */

package accesoDatos.memoria;

import java.util.ArrayList;
import java.util.List;

import accesoDatos.DatosException;
import accesoDatos.OperacionesDAO;
import accesoDatos.memoria.IndexSortTemplate;
import modelo.Identificable;
import modelo.Sesion;

public class SesionesDAO extends IndexSortTemplate implements OperacionesDAO {
	
	// Singleton
	private static SesionesDAO instance;
	
	private ArrayList<Identificable> datosSesiones;

	public SesionesDAO() {
		datosSesiones = new ArrayList<>();
	}

	public static SesionesDAO getInstance() {
		if (instance == null) {
			instance = new SesionesDAO();

		}
		return instance;
	}

	public List<Identificable> getSesion() {
		return datosSesiones;
	}

	public Sesion consultar(String id) {
		assert id != null;
		int indice = indexSort(datosSesiones, id);

		if (indice >= 0) {
			return (Sesion) datosSesiones.get(indice);
		}

		return null;
	}

	public List<Identificable> consultarTodos() {
		return datosSesiones;
	}
	
	public List<Identificable> consultarTodasUsuario(String idUsr) {
		return datosSesiones;
	}

	public void alta(Object obj) throws DatosException {
		Sesion sesion = (Sesion)obj;
		assert sesion != null;
		int posicionInsercion = indexSort(datosSesiones, sesion.getId());

		if (posicionInsercion < 0) {
			datosSesiones.add(Math.abs(posicionInsercion) - 1, sesion);
		} else {
			throw new DatosException("Alta Sesion: ya existente");
		}
	}

	public Sesion baja(String id) throws DatosException {
		assert id != null;
		int posicion = indexSort(datosSesiones, id);
		
		if (posicion > 0) {
			return (Sesion) datosSesiones.remove(posicion-1);
		}
		else {
			throw new DatosException("Baja : El usuario no existe");
		}
	}

	public Sesion actualizar(Object obj) throws DatosException {
		assert obj != null;
		Sesion sesion = (Sesion)obj;
		int posicion = indexSort(datosSesiones, sesion.getId());
		
		if (posicion > 0) {
			datosSesiones.set(posicion-1, (Identificable) sesion);	
		}
		else {
			throw new DatosException("Sesiones : " + sesion.getId() + " no existe");
		}
		return sesion;
	}

	public String toStringDatos() {
		StringBuilder sb = new StringBuilder();

		for (Identificable sesiones : datosSesiones) {
			sb.append("\n" + sesiones);
		}
		return sb.toString();
	}

	public String toStringIds() {
		StringBuilder sb = new StringBuilder();
		
		for (Identificable sesiones : datosSesiones) {
			sb.append(sesiones + "\n");
		}
		return sb.toString();
	}

	public void borrarTodo() {
		datosSesiones.clear();
		
	}

}
