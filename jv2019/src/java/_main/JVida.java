/** 
Proyecto: Juego de la vida.
 * Implementa el control de inicio de sesión y ejecución de la simulación por defecto. 	
 * @since: prototipo 0.1.0
 * @source: JVida.java 
 * @version: 0.2.0 - 2020/04/29
 * @author: ajp
 * @author:  
 */

package _main;

import accesoDatos.DatosFacade;
import accesoUsr.Presentacion;
import config.Configuracion;
import modelo.Simulacion;

public class JVida {

	private static Presentacion interfazUsr;
	private static DatosFacade almacen;
	
	public JVida() {
		interfazUsr = new Presentacion();
		almacen = new DatosFacade();
	}
	
	/**
	 * Secuencia principal del programa.
	 */
	public static void main(String[] args) {		
		
		new JVida();	
		System.out.println(almacen.toStringDatosUsuarios());
		
		if (interfazUsr.inicioSesionCorrecto()) {
					
			System.out.println("Sesión iniciada por: " 
					+ interfazUsr.getSesion().getUsr().getNombre() + " " 
					+ interfazUsr.getSesion().getUsr().getApellidos());	
			
			Simulacion demo = almacen.consultarSimulacion(Configuracion.get().getProperty("simulacion.nombrePredeterminado"));
			interfazUsr.setSimulacion(demo);
			demo.getMundo().lanzarSimulacion(interfazUsr);
		}
		else {
			System.out.println("\nDemasiados intentos fallidos...");
		}		
		System.out.println("Fin del programa.");
	}

} 
