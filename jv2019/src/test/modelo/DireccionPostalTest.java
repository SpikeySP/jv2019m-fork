/** Proyecto: Juego de la vida.
 *  Prueba de la clase DireccionPostal según el modelo 0.2.0
 *  @since: prototipo0.1.1
 *  @source: DireccionPostalTest.java 
 *  @version: 0.2.0 - 2020/04/28
 *  @author: Jose Antonio Diaz Miralles - Grupo 3
 */

package modelo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class DireccionPostalTest {
	private static DireccionPostal direccion1; 
	private DireccionPostal direccion2; 

	/**
	 * Método que se ejecuta antes de cada @Test para preparar datos de prueba.
	 */
	@BeforeAll
	public static void iniciarlizarDatosFijos() {
		// Objetos no modicados en las pruebas.
		direccion1 = new DireccionPostal("Calle Prueba",
							"5",
							"30800",
							"Poblacion");
	}

	/**
	 * Método que se ejecuta una sola vez al final del conjunto pruebas.
	 * No es necesario en este caso.
	 */
	@AfterAll
	public static void limpiarDatosFijos() {
		direccion1 = null;
	}

	/**
	 * Método que se ejecuta antes de cada pruebas.
	 */
	@BeforeEach
	public void iniciarlizarDatosVariables() {	
		direccion2 = new DireccionPostal();
	}

	/**
	 * Método que se ejecuta después de cada @Test para limpiar datos de prueba.
	 */
	@AfterEach
	public void borrarDatosPrueba() {
		direccion2 = null;
	}

	// Test's CON DATOS VALIDOS
	
	@Test
	public void testSetCalle() {
		direccion2.setCalle("Calle Larga");
		assertEquals(direccion2.getCalle(), "Calle Larga");
	}
	
	@Test
	public void testSetNumero() {
		direccion2.setNumero("45");
		assertEquals(direccion2.getNumero(), "45");
	}
	
	@Test
	public void testSetCp() {
		direccion2.setCp("30840");
		assertEquals(direccion2.getCp(), "30840");
	}
	
	@Test
	public void testSetPoblacion() {
		direccion2.setPoblacion("Granada");
		assertEquals(direccion2.getPoblacion(), "Granada");
	}

	@Test
	public void testToString() {
		assertEquals(direccion1.toString(), 
				"Calle Prueba" +
				"5" +
				"30800" +
				"Poblacion"		
			);
	}

	// Test's CON DATOS NO VALIDOS

	@Test
	public void testSetCalleNull() {
		try {
			direccion2.setCalle(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}
	
	@Test
	public void testSetCalleNoValida() {
		try {
			direccion2.setCalle("n0 valid4");
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}
	
	@Test
	public void testSetNumeroNull() {
		try {
			direccion2.setNumero(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}
	
	@Test
	public void testSetNumeroNoValido() {
		try {
			direccion2.setNumero("veinte");
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}
	
	@Test
	public void testSetCpNull() {
		try {
			direccion2.setCp(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}
	
	@Test
	public void testSetCpNoValido() {
		try {
			direccion2.setCp("777777777777777777");
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}
	
	@Test
	public void testSetPoblacionNull() {
		try {
			direccion2.setPoblacion(null);
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}
	
	@Test
	public void testSetPoblacionNoValida() {
		try {
			direccion2.setPoblacion("Murc1@");
			fail("No debe llegar aquí...");
		} 
		catch (AssertionError e) { 
		}
	}

} // class