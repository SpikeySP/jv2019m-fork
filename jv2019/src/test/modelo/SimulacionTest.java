/** 
 * Proyecto: Juego de la vida.
 *  Prueba Junit5 de la clase Simulacion segun el modelo 1.2
 *  @since: prototipo 0.1.0
 *  @source: SimulacionTest.java 
 *  @version: 0.1.2 - 2020/05/07
 *  @author: josemaelmismo (Jose Manuel Monteagudo) GRUPO 4
 *  @author: SpikeySP (Juan Ignacio Riquelme Martinez) GRUPO 4
 */

package modelo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import modelo.Usuario.RolUsuario;
import util.Fecha;

public class SimulacionTest {

	private static Usuario usr;
	private static Fecha fecha;
	private static Mundo mundo;
	private static Simulacion simulacion1;
	private Simulacion simulacion2;

	/**
	 * Metodo que se ejecuta una sola vez al principio del conjunto pruebas.
	 */
	@BeforeAll
	public static void iniciarlizarDatosFijos() {
		// Objetos no modicados en las pruebas.
		try {
			usr = new Usuario(new Nif("00000000A"), "Jose", "Riquelme Serrano",
					new DireccionPostal("Roncal", "5", "30152", "Murcia"), new Correo("jose@gmail.com"),
					new Fecha(2000, 01, 20), new Fecha(2018, 10, 15), new ClaveAcceso("Miau#12"), RolUsuario.NORMAL);
			fecha = new Fecha(2018, 10, 20, 10, 35, 2);
			mundo = new Mundo();
			simulacion1 = new Simulacion(usr, fecha, mundo);

		} catch (ModeloException e) {

		}
	}

	@AfterAll
	public static void borrarDatosPrueba() {
		simulacion1 = null;
	}

	@BeforeEach
	public void iniciarlizarDatosVariables() {
		try {
			simulacion2 = new Simulacion();

		} catch (ModeloException e) {

		}
	}

	// Test con datos validos
	@Test
	public void testSimulacionConvencional() {
		assertSame(simulacion1.getUsr(), usr);
		assertSame(simulacion1.getFecha(), fecha);
		assertSame(simulacion1.getMundo(), mundo);
	}

	@Test
	public void testSimulacionDefecto() {
		try {
			assertEquals(simulacion2.getUsr().getNif(), new Usuario().getNif());
			assertEquals(simulacion2.getFecha().getYear(), new Fecha().getYear());
			assertEquals(simulacion2.getFecha().getMes(), new Fecha().getMes());
			assertEquals(simulacion2.getFecha().getDia(), new Fecha().getDia());
			assertNotNull(simulacion2.getMundo());

		} catch (ModeloException e) {

		}
	}

	@Test
	public void testSimulacionCopia() {
		assertNotSame(simulacion2, new Simulacion(simulacion2));
	}

	@Test
	public void testSetUsr() {
		simulacion2.setUsr(usr);
		assertSame(simulacion2.getUsr(), usr);
	}

	@Test
	public void testSetMundo() {
		simulacion2.setMundo(mundo);
		assertSame(simulacion2.getMundo(), mundo);
	}

	@Test
	public void testSetFecha() {
		simulacion2.setFecha(fecha);
		assertSame(simulacion2.getFecha(), fecha);
	}

	@Test
	public void testToString() {
		assertNotNull(simulacion1.toString());
	}

	@Test
	public void testSetUsrNull() {
		try {
			simulacion2.setUsr(null);
			fail("No debe llegar aqui...");
		} catch (AssertionError e) {
			assertTrue(simulacion2.getUsr() != null);
		}
	}

	@Test
	public void testSetMundoNull() {
		try {
			simulacion2.setMundo(null);
			fail("No debe llegar aqui...");
		} catch (AssertionError e) {
			assertTrue(simulacion2.getMundo() != null);
		}
	}

	@Test
	public void testSetFechaNull() {
		try {
			simulacion2.setFecha(null);
			fail("No debe llegar aqui...");
		} catch (AssertionError e) {
			assertTrue(simulacion2.getFecha() != null);
		}
	}

}
